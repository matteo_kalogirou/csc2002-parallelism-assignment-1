/**
 * Method used to sum the resulting array in parallel using multiple threads
 * and the divide and conquer method.
 * 
 * Returns the sum, not the average.
 */

//  package src;

import java.util.*;
import java.util.concurrent.RecursiveTask;

 public class SumArrayParallel extends RecursiveTask<Float>
 {
     private int lo;
     private int hi;
     private float[] arr;
     private int SEQ_CUT;

     float sum;

     SumArrayParallel(int lo,int hi, float[] arr, int SEQ_CUT){
         this.lo = lo;
         this.hi = hi;
         this.arr = arr;
         this.SEQ_CUT = SEQ_CUT;
     }

     protected Float compute()
     {
         //Sequential Run
         if(hi-lo < SEQ_CUT){
             sum = 0;
             for(int i = lo; i < hi; i++)
                 sum += arr[i];
            
                 return sum;
         }else{
             //Parallel style, bisect the workload and offload half to a concurrent thread
             SumArrayParallel left = new SumArrayParallel(lo, (lo+hi)/2, arr, SEQ_CUT);
             SumArrayParallel right = new SumArrayParallel((hi+lo)/2, hi, arr, SEQ_CUT);

             left.fork();
             float rightSum = right.compute();
             float leftSum = left.join();
             return leftSum + rightSum;
         }
     }


 }