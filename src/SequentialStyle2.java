/**
 * Second attempt to make the program not take forever to work
 */


import java.io.*;
import java.util.*;

 public class SequentialStyle2{

    //instance variables
    static int LANDSCAPE_X;
    static int LANDSCAPE_Y;
    static float landscape[][];
    //static Tree trees[];
    static int NUM_TREES;

    public static void main(String[] args)
    {

        

        //Create a scanner object
        Scanner input = null;
        try{
            input = new Scanner(new FileInputStream("../data/sample_input.txt"));
            // input = new Scanner(new FileInputStream("sample_input.txt"));
        }
        catch(FileNotFoundException e)
        {
            System.out.println("File not found or does not exist.");
            System.exit(0);
        }

        //Read in the data
        LANDSCAPE_X = input.nextInt();
        LANDSCAPE_Y = input.nextInt();
        input.nextLine();

        System.out.println("The landscape size is: "+LANDSCAPE_X+" by "+LANDSCAPE_Y);

        landscape = new float[LANDSCAPE_X][LANDSCAPE_Y];
        //Populate the landscape matrix
        for(int i = 0; i<LANDSCAPE_X; i++){
            for(int j = 0; j<LANDSCAPE_Y; j++)
            {
                if(input.hasNextInt())
                    landscape[i][j] = input.nextFloat();
            }
            System.out.println(landscape[i][0]);
        }

        System.out.println("The last number in the matrix is : "+ landscape[LANDSCAPE_X-1][LANDSCAPE_Y-1]);

        //populate the tree data
        input.nextLine();
        NUM_TREES = input.nextInt();
        System.out.println("Number of trees: "+ NUM_TREES);
        Tree trees[] = new Tree[NUM_TREES];




    }
 }