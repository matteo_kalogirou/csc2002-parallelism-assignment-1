/**
 * Thid program is used to calculate the average amount of sunlight each tree gets as well as the total sunlight for each tree.
 * 
 * Program reads two pieces of information: 1) Data describing the average sunlight distributed across the area
 *                                          2) Position of each tree in the area and the size of the tree
 * 
 * Required to calculate:   - the total amount of sunlight for each tree 
 *                          - the average amount of sunlight
 * Matteo Kalogirou
 */

 import java.util.*;
 import java.io.*;

 public class Seq
 {
     //Global Instance variables
    static int X_SIZE;
    static int Y_SIZE;
    static int NUM_TREES;
    static float landscape[][];
    static long startTime;
    //static float treeRes[];

    public static void main(String[] args)

    {

        //Begin by reading in the data
        //Create a scanner object open the data file
        Scanner input = null;
        try{    
            input = new Scanner(new FileInputStream("../data/generatedLandscape_6000by60000_2000000.txt"));
            input.useLocale(Locale.US);
        }catch(FileNotFoundException e){
            System.out.println("File not found or could not be opened!");
            System.exit(0);
        }

        //Read in the first two numbers:
        X_SIZE = input.nextInt();
        Y_SIZE = input.nextInt();
        input.nextLine();
        
        //Create a float matrix to store the landscape data
        landscape = new float[X_SIZE][Y_SIZE];
        //for each float, store them sequentially in the matrix
        for(int i = 0; i<X_SIZE; i++)
            for(int j = 0; j<Y_SIZE; j++)
            {
                if(input.hasNextFloat())
                landscape[i][j] = input.nextFloat();
            }
        input.nextLine();

        //Read in the tree data
        NUM_TREES = input.nextInt();
        input.nextLine();
        Tree trees[] = new Tree[NUM_TREES];
        for(int k=0; k<NUM_TREES; k++)
        {
            trees[k] = new Tree(input.nextInt(), input.nextInt(), input.nextInt());
            input.nextLine();            
        }
        input.close();
        System.out.println("Read in the Data. Starting Computation...");
        System.gc();
        //Time to instantiate all of the result arrays
        float[] treeRes = new float[NUM_TREES];

        //                  ******TIME START******
            int iter = 1;
            float time[] = new float[iter];
            float averageSunlight;
        for(int x = 0; x < iter; x ++)
        {
            System.gc();
            tick();
            //Fill the resulting array
            for(int i=0; i< trees.length; i++)
            {
                treeRes[i] = getTreesSunlight(trees[i]);
            }
            //Calculate the average sunlight for a tree in the area
            averageSunlight = getAverage(treeRes);
            time[x] = tock();
            System.out.println("Done iteration: "+x);
        }
        //                  ******TIME STOP******
        
        //Print the results
        //PrintWriter output = null;
        PrintWriter op2 = null;
        try{
          //  output = new PrintWriter(new FileOutputStream("../data/seq_result.txt"));
            op2 = new PrintWriter(new FileOutputStream("../data/outputs/seq_time_6000.txt"));
        }catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file");
            System.exit(0);
        }

        System.out.println("Printing the results...");
        op2.println("Timing Results:");
        op2.println("Average time: "+getAverage(time));
        for (float x : time) {
            op2.println(x);
        }
        op2.close();

        // output.println(getAverage(treeRes));
        // output.println(NUM_TREES);
        // for (float s : treeRes) {
        //     output.println(s);
        // }
        // System.out.println("Complete!");
        // output.close();




    }//=================================END MAIN=======================================

    /**
     * Timing methods used to measure the time taken for a process
     */
    private static void tick()
    {
        startTime = System.currentTimeMillis();
    }

    private static float tock()
    {
        return (System.currentTimeMillis()-startTime)/1000.0f;
    }

    /**
     * Method to calculate the total sunlight for each tree according to its size
     * @param tree A tree object holding the x y and size of the tree
     * @return A NUM_TREES x 1 array storing the total sunlight for each tree
     */
    private static float getTreesSunlight(Tree tree)
    {
        float res = 0;
        //Sum all of the points taken up by the tree
        for(int i=tree.getX(); i<tree.getX()+tree.getSize(); i++ )
            for(int j = tree.getY(); j<tree.getY()+tree.getSize(); j++)
            {
                //If the point is on the map then add it
                if(i < X_SIZE && j < Y_SIZE)
                    res += landscape[i][j];
            }
        
        return res;
    }

    /**
     * Method used to calculate the average of an array
     * @param arr The array of type float that stores all data
     * @return The average in float format
     */
    private static float getAverage(float[] arr)
    {
        float sum = 0;

        for(int i=0; i<arr.length; i++)
            sum += arr[i];

        return sum/arr.length;
    }

 }//END CLASS