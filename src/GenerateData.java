/**
 * Creates data in the same format as the sample data but of any size and writes this to a file
 */

 import java.util.*;
 import java.io.*;
 import java.math.*;

 public class GenerateData
 {
     //instance variables
     private int X;
     private int Y;
     private int NUM_TREES;
     private float[] randFloats;
     private Tree[] randTrees;

     //constructor
     //x = xsize, y=ysize, num=numOfTrees
     public GenerateData(int x, int y, int num)
     {
         X = x;
         Y= y;
         NUM_TREES = num;
     }

     public void makeData()
     {
        randFloats = new float[X*Y];
        genLandscape(randFloats);

        randTrees = new Tree[NUM_TREES];
        genTree(randTrees);

        //Write data to a file:
        PrintWriter op = null;
        String filename = "../data/generatedLandscape_"+X+"by"+Y+"_"+NUM_TREES+".txt";
        try{
            op = new PrintWriter(new FileOutputStream(filename));
        }catch(FileNotFoundException e)
        {
            System.out.println("Could not open fie.");
            System.exit(0);
        }

        op.println(X+" "+Y);
        for (float f: randFloats){
            op.printf(Locale.ROOT, "%1.2f ", f);
        }
        op.println();

        op.println(NUM_TREES);

        for (Tree t: randTrees){
            op.println(t.toString());
        }

        op.close();

     }

     //Method to print out the data.

     //Method to construct random float value 
     private float genFloat(float min, float max)
     {
        //code
        return (float) ((Math.random()*((max-min)+1)) + min);
     }

     private int genInt(int min, int max)
     {
        return (int) ((Math.random()*((max-min)+1)) + min);
     }

     private void genTree(Tree[] t)
     {
        for (int i = 0; i < NUM_TREES; i++)
        {
            t[i] = new Tree(genInt(0, X-1), genInt(0, Y-1), genInt(1, X/4));
        }
     }

    private void genLandscape(float[] rand)
    {
        float f = (float)10.00;
        for(int i =0; i< X*Y; i++)
            rand[i] = genFloat(0, f);
    }

    
 }