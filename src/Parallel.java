/**
 * Thid program is used to calculate the average amount of sunlight each tree gets as well as the total sunlight for each tree.
 * Additionally, the progarm will be used to test the effects of different sequential cutoffs, data sizes and machine architectures
 * when calcualting the results.
 * 
 * Program reads two pieces of information: 1) Data describing the average sunlight distributed across the area
 *                                          2) Position of each tree in the area and the size of the tree
 * 
 * Required to calculate:   - the total amount of sunlight for each tree 
 *                          - the average amount of sunlight
 * Matteo Kalogirou
 */

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.io.*;
import java.math.*;

 public class Parallel
 {
    //Global Instance variables
     static int X_SIZE;
     static int Y_SIZE;
     static int SEQUENTIAL_CUTOFF=0;
     protected static int NUM_TREES;
     protected static float landscape[][];
     protected static long startTime;
     protected static float treeRes[];
     static final ForkJoinPool fjpool = new ForkJoinPool();

    public static void main(String[] args)
    {
        Scanner input = null;
        try{    
            input = new Scanner(new FileInputStream("../data/sample_input.txt"));
            input.useLocale(Locale.US);
        }catch(FileNotFoundException e){
            System.out.println("File not found or could not be opened!");
            System.exit(0);
        }

        //Read in the first two numbers:
        X_SIZE = input.nextInt();
        Y_SIZE = input.nextInt();
        input.nextLine();
        
        //Create a float matrix to store the landscape data
        landscape = new float[X_SIZE][Y_SIZE];
        //for each float, store them sequentially in the matrix
        for(int i = 0; i<X_SIZE; i++)
            for(int j = 0; j<Y_SIZE; j++)
            {
                if(input.hasNextFloat())
                landscape[i][j] = input.nextFloat();
            }
        input.nextLine();

        //Read in the tree data
        NUM_TREES = input.nextInt();
        input.nextLine();
        Tree trees[] = new Tree[NUM_TREES];
        for(int k=0; k<NUM_TREES; k++)
        {
            trees[k] = new Tree(input.nextInt(), input.nextInt(), input.nextInt());
            input.nextLine();            
        }
        input.close();
        //cleanup unused system memory
        System.gc();

        int loopNum = 15;
        for(int z = 1; z < loopNum; z++){
            SEQUENTIAL_CUTOFF = (int)Math.pow(2, z);
            System.out.println("Sequential Cutoff = "+SEQUENTIAL_CUTOFF);
            System.out.println("Data read in. Begining Computation...");

            float averageSunlight = 0;
            int iter = 12;
            float temp;
            float[] time = new float[iter-5];   //allow 5 iterations for the system to warm up
            treeRes = new float[NUM_TREES];

            for(int i = 0; i< iter; i++){
                System.gc();
                tick();
                //Calculate the total sunlight exposure for each tree
                temp = fjpool.invoke(new SumSunlight(0, trees.length, trees, SEQUENTIAL_CUTOFF));
                //Calculate the average sunlight for each tree
                averageSunlight = getAverageSunlight(treeRes);
                temp = tock();
                if(i >=5)
                    time[i-5] = temp;
            }

            //System.out.println("Average sunlight: "+averageSunlight);

            String timeFilename = "../data/par_result"+z+".txt";

            //Writing the results to a file
            PrintWriter op = null;
            PrintWriter opt = null;
            try{
                op = new PrintWriter(new FileOutputStream("../data/par_result.txt"));
                opt = new PrintWriter(new FileOutputStream(timeFilename));
            }catch(FileNotFoundException e)
            {
                System.out.println("Error opening the file");
                System.exit(0);
            }

            System.out.println("Printing the results...");
            opt.println("Timing Results: ");
            opt.println("Sequential Cutoff = "+SEQUENTIAL_CUTOFF);
            opt.println("Average time = "+getAverageSunlight(time)+"s");
            opt.println();
            for (float x : time) {
                opt.println(x);
             }
            opt.close();

            op.println(averageSunlight);
            op.println(NUM_TREES);
            for (float s : treeRes) {
                op.println(s);
            }
            System.out.println("Complete!");
            op.close();
    }//end loop

    }//END MAIN


    /**
     * Method which uses divide and conquer to sum an array in parallel and return the average.
     * @param arr Of floats to sum and average
     * @return the average of the array
     */
    private static float getAverageSunlight(float[] arr)
    {
        return fjpool.invoke(new SumArrayParallel(0, arr.length, arr, SEQUENTIAL_CUTOFF))/arr.length;
    }

    private static void tick(){
		startTime = System.currentTimeMillis();
	}
	private static float tock(){
		return (System.currentTimeMillis() - startTime) / 1000.0f; 
	}


 }