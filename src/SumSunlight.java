/**
 * Recursive thread method. Uses the divide and conquer principle to calculate the amount of sunlight for each tree
 */

 import java.util.concurrent.RecursiveTask;

 public class SumSunlight extends RecursiveTask<Integer>
 {
     final int SEQ_CUT;
     int lo;
     int hi;
     Tree[] arr;
     float sum;

     public SumSunlight(int lo, int hi, Tree[] arr, int SEQ_CUT)
     {
         this.lo = lo;
         this.hi = hi;
         this.arr = arr;
         this.SEQ_CUT = SEQ_CUT;
     }

     public Integer compute()
     {
         //Sequential execution
         if( (hi-lo) < SEQ_CUT){
             //In the trees
             for(int i = lo; i < hi; i++)
             {
                sum = 0;
                for(int x = arr[i].getX(); x< (arr[i].getX()+arr[i].getSize()); x++ )
                    for(int y = arr[i].getY(); y< (arr[i].getY()+arr[i].getSize()) ; y++ ){
                        if(x < Parallel.X_SIZE && y< Parallel.Y_SIZE)
                            sum += Parallel.landscape[x][y];
                    }
                    Parallel.treeRes[i] = sum;
             }

         }else{
             SumSunlight left = new SumSunlight(lo, (hi+lo)/2 , arr, SEQ_CUT);
             SumSunlight right = new SumSunlight((hi+lo)/2, hi, arr, SEQ_CUT);

             left.fork();
             right.compute();
             left.join();
         }
         return 1;
     }//end COMPUTE

 }//end class