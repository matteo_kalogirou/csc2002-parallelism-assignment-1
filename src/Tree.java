/**
 * This is a tree class which stores the tree coordinates and the size of the tree
 */

 public class Tree
 {
        //Instance variables
        private int x;
        private int y;
        private int size;

        //contructor
        public Tree(int x, int y, int size){
            this.x = x;
            this.y = y;
            this.size= size;
        }

        public int getX() {return x;}
        public int getY() {return y;}
        public int getSize() {return size;}

        public String toString()
        {
            return x+" "+y+" "+size;
        }
        
 }
