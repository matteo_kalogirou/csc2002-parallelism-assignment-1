
import java.io.*;
import java.util.*;

public class testMakeData
{
    public static void main(String[] args)
    {
        GenerateData d = new GenerateData(10, 10, 4);
        d.makeData();

        GenerateData e = new GenerateData(100, 100, 500);
        e.makeData();

        GenerateData f = new GenerateData(1500, 1500, 500000);
        f.makeData();

        GenerateData g = new GenerateData(6000, 60000, 2000000);
        g.makeData();
    }
}