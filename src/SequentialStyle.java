/**
 * Script used to assess the speedup of the parallel version of this program.
 * Program reads two pieces of information: 1) Data describing the average sunlight distributed across the area
 *                                          2) Position of each tree in the area and the size of the tree
 * 
 * Required to calculate:   - the total amount of sunlight for each tree 
 *                          - the average amount of sunlight
 */


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class SequentialStyle
{
    //instance variables
    static float sunMap[][] = null;            //Stores the distribution of sunlight in the area           [nxn]
    static int treeMap[][] = null;             //Stores the location and size of each tree in the area     [mx3] [x y size]
    static float treeRes[] = null;             //Stores the calculated result for each tree                [m]
    static int SUNMAPSIZE_X;                    
    static int SUNMAPSIZE_Y;
    static long startTime;
    static final int ITERATIONS = 10;

    //                                  ====MAIN====
    public static void main(String[] args)
    {
        //Read in the data
        Scanner inputStream = null;
        
        try{
            inputStream = new Scanner(new FileInputStream("sample_input.txt"));
        }
        catch(FileNotFoundException e)
        {
            System.out.println("File could not be read or was not found.");
            System.exit(0);
        }

        SUNMAPSIZE_X = inputStream.nextInt();
        SUNMAPSIZE_Y = inputStream.nextInt();
        System.out.println("The size of the sun map is: " + SUNMAPSIZE_X+" by "+SUNMAPSIZE_Y);
        inputStream.nextLine();

        //Store the sunmap data into a matrix
        sunMap = new float[SUNMAPSIZE_X][SUNMAPSIZE_Y];

        String temp = inputStream.nextLine();
        String tempSplit[] = new String[SUNMAPSIZE_X*SUNMAPSIZE_Y];
        tempSplit = temp.split(" ");
        int counter = 0;
        
        for(int i=0; i<SUNMAPSIZE_X; i++){
            for(int j = 0; j<SUNMAPSIZE_Y; j++){
                sunMap[i][j] = Float.parseFloat(tempSplit[counter]);
                counter++;
            }
        }

        //System.out.println("Last entry is: "+ sunMap[2999][2999]);

        //Store the tree map data
        int NUMBER_OF_TREES = inputStream.nextInt();
        inputStream.nextLine();

        System.out.println("There are "+ NUMBER_OF_TREES+" trees on the map.");
        treeMap = new int[NUMBER_OF_TREES][3];
        // temp = "";
        // tempSplit = new String[3];

        for(int i = 0; i<NUMBER_OF_TREES; i++){
            temp = inputStream.nextLine();
            tempSplit = temp.split(" ");
            treeMap[i][0] = Integer.parseInt(tempSplit[0]);          //x corner of tree
            treeMap[i][1] = Integer.parseInt(tempSplit[1]);          //y corner of tree
            treeMap[i][2] = Integer.parseInt(tempSplit[2]);          //size of tree

            // treeMap[i][0] = inputStream.nextInt();          //x corner of tree
            // treeMap[i][1] = inputStream.nextInt();          //y corner of tree
            // treeMap[i][2] = inputStream.nextInt();          //size of tree
            // if(inputStream.hasNextLine())
            //     inputStream.nextLine();

            System.out.println(""+ i);
        }

        System.out.println("Last Tree is: "+ treeMap[NUMBER_OF_TREES-1][0]);

        inputStream.close();                //Finished reading in data

        //Instantiate all result variables
        treeRes = new float[NUMBER_OF_TREES];
        float averageSunlight = 0;
        float time[] = new float[ITERATIONS];
        int iter = 0;

        //Begin calculating the amount of sunlight for each tree
        while(iter<10)
        {
            tick();
            for(int i = 0; i<NUMBER_OF_TREES; i++)
            {
                 treeRes[i] = getTreeSunlight(treeMap[i][0], treeMap[i][1], treeMap[i][2]);
            }
            //Calculate the average sunlight per tree
            averageSunlight = getAverage(treeRes);
            time[iter] = tock();
           
            System.out.println("The program took "+time[iter]+" seconds to complete.");
            System.out.println("");
            System.out.println("Results:");
            System.out.println("");

            System.out.println(""+ averageSunlight);
            System.out.println(""+NUMBER_OF_TREES);
            for (float x : treeRes) {
                System.out.println(""+x);
            }

            iter++;
        }

    }

    //-------------------------------------------------Methods-----------------------------------------------------

    /**
     * Method used to calculate the amount of sunlight received by each tree
     * @param x The x corner of the tree location
     * @param y The y corner of the tree location
     * @param size The size of the tree (square)
     * @return The total sunlight received by the tree
     */
    private static float getTreeSunlight(int x, int y, int size)
    {
        float res = 0;
        for(int i = x; i<x+size; i++)
            for(int j=y; j<size; j++)
            {
                if(i < (SUNMAPSIZE_X-1) && j < (SUNMAPSIZE_Y-1))
                {
                    res += sunMap[i][j];   
                }else{ continue;}
            }
        return res;
    }

    /**
     * Method used to average the result of an array sequenytial style
     * @param arr The array to average
     * @return The average of the array
     */
    private static float getAverage(float arr[])
    {
        float sum = 0;
        for(int i = 0; i<arr.length; i++)
            sum+=arr[i];             

        return sum/arr.length;
    }

    /**
     * Timing methods used to measure the time taken for a process
     */
    private static void tick()
    {
        startTime = System.currentTimeMillis();
    }

    private static float tock()
    {
        return (System.currentTimeMillis()-startTime)/1000.0f;
    }



}